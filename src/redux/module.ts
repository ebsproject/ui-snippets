export function reduxModule(){
    return`/*
 Initial state and properties
 */
export const initialState = {}
/*
 Action types
 */
const NEW_ACTION = 'NEW_ACTION'
/*
 Arrow function for change state
 */
const setData = (payload) => ({
  type: NEW_ACTION,
  payload,
})
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case NEW_ACTION:
      return {
        ...state,
        propertyName: payload,
      }
    default:
      return state
  }
}
`;
};
