// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { atom, indexAtom } from "./components/atoms";
import { grid, indexGrid } from "./components/grid";
import { molecule, indexMolecule } from "./components/molecules";
import { organism, indexOrganism } from "./components/organisms";
import { test } from "./components/test";
import { page, indexPage } from "./pages/page";
import { reduxModule } from "./redux/module";
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "ebs-snippets" is now active!');

  let cancelToken = new vscode.CancellationTokenSource();

  //For Create Atoms
  context.subscriptions.push(
    vscode.commands.registerCommand(
      "fs.newAtom",
      async (rootUri: vscode.Uri, _) => {
        let options = {
          ignoreFocusOut: true,
          placeHolder: "Enter Atom Name",
        };
        let input = vscode.window.showInputBox(options, cancelToken.token);
        input.then((res) => {
          if (res) {
            const name = Validate(res);
            const directoryName = vscode.Uri.parse(`${rootUri}/${name}`);
            const fileName = vscode.Uri.parse(`${rootUri}/${name}/${name}.js`);
            const testFile = vscode.Uri.parse(`${rootUri}/${name}/${name}.test.js`);
            const fileIndex = vscode.Uri.parse(`${rootUri}/${name}/index.js`);
            vscode.workspace.fs.createDirectory(directoryName);
            vscode.workspace.fs.writeFile(fileName, Buffer.from(atom(name ? name : "")));
            vscode.workspace.fs.writeFile(testFile, Buffer.from(test(name ? name : "")));
            vscode.workspace.fs.writeFile(
              fileIndex,
              Buffer.from(indexAtom(name ? name : ""))
            );
          }
        });
      }
    )
  );
  //For Create Grid
  context.subscriptions.push(
    vscode.commands.registerCommand(
      "fs.newGrid",
      async (rootUri: vscode.Uri, _) => {
        let options = {
          ignoreFocusOut: true,
          placeHolder: "Enter Grid Name"
        };
        let input = vscode.window.showInputBox(options, cancelToken.token);
        input.then((res) => {
          if (res) {
            const name = Validate(res);
            const directoryName = vscode.Uri.parse(`${rootUri}/${name}`);
            const fileName = vscode.Uri.parse(`${rootUri}/${name}/${name}.js`);
            const testFile = vscode.Uri.parse(`${rootUri}/${name}/${name}.test.js`);
            const fileIndex = vscode.Uri.parse(`${rootUri}/${name}/index.js`);
            vscode.workspace.fs.createDirectory(directoryName);
            vscode.workspace.fs.writeFile(fileName, Buffer.from(grid(name ? name : "")));
            vscode.workspace.fs.writeFile(testFile, Buffer.from(test(name ? name : "")));
            vscode.workspace.fs.writeFile(
              fileIndex,
              Buffer.from(indexGrid(name ? name : ""))
            );
          }
        });
      }
    )
  );
  //For Create Molecules
  context.subscriptions.push(
    vscode.commands.registerCommand(
      "fs.newMolecule",
      async (rootUri: vscode.Uri, _) => {
        let options = {
          ignoreFocusOut: true,
          placeHolder: "Enter Molecule Name",
        };
        let input = vscode.window.showInputBox(options, cancelToken.token);
        input.then((res) => {
          if (res) {
            const name = Validate(res);
            const directoryName = vscode.Uri.parse(`${rootUri}/${name}`);
            const fileName = vscode.Uri.parse(`${rootUri}/${name}/${name}.js`);
            const testFile = vscode.Uri.parse(`${rootUri}/${name}/${name}.test.js`);
            const fileIndex = vscode.Uri.parse(`${rootUri}/${name}/index.js`);
            vscode.workspace.fs.createDirectory(directoryName);
            vscode.workspace.fs.writeFile(fileName, Buffer.from(molecule(name ? name : "")));
            vscode.workspace.fs.writeFile(testFile, Buffer.from(test(name ? name : "")));
            vscode.workspace.fs.writeFile(
              fileIndex,
              Buffer.from(indexMolecule(name ? name : ""))
            );
          }
        });
      }
    )
  );
  //For Create Organisms
  context.subscriptions.push(
    vscode.commands.registerCommand(
      "fs.newOrganism",
      async (rootUri: vscode.Uri, _) => {
        let options = {
          ignoreFocusOut: true,
          placeHolder: "Enter Organism Name",
        };
        let input = vscode.window.showInputBox(options, cancelToken.token);
        input.then((res) => {
          if (res) {
            const name = Validate(res);
            const directoryName = vscode.Uri.parse(`${rootUri}/${name}`);
            const fileName = vscode.Uri.parse(`${rootUri}/${name}/${name}.js`);
            const testFile = vscode.Uri.parse(`${rootUri}/${name}/${name}.test.js`);
            const fileIndex = vscode.Uri.parse(`${rootUri}/${name}/index.js`);
            vscode.workspace.fs.createDirectory(directoryName);
            vscode.workspace.fs.writeFile(fileName, Buffer.from(organism(name ? name : "")));
            vscode.workspace.fs.writeFile(testFile, Buffer.from(test(name ? name : "")));
            vscode.workspace.fs.writeFile(
              fileIndex,
              Buffer.from(indexOrganism(name ? name : ""))
            );
          }
        });
      }
    )
  );
  //For Create Pages
  context.subscriptions.push(
    vscode.commands.registerCommand(
      "fs.newPage",
      async (rootUri: vscode.Uri, _) => {
        let options = {
          ignoreFocusOut: true,
          placeHolder: "Enter Page Name",
        };
        let input = vscode.window.showInputBox(options, cancelToken.token);
        input.then((res) => {
          if (res) {
            const name = Validate(res);
            const directoryName = vscode.Uri.parse(`${rootUri}/${name}`);
            const fileName = vscode.Uri.parse(
              `${rootUri}/${name}/${name}.js`
            );
            const testFile = vscode.Uri.parse(`${rootUri}/${name}/${name}.test.js`);
            const fileIndex = vscode.Uri.parse(`${rootUri}/${name}/index.js`);
            vscode.workspace.fs.createDirectory(directoryName);
            vscode.workspace.fs.writeFile(fileName, Buffer.from(page(name ? name : "")));
            vscode.workspace.fs.writeFile(testFile, Buffer.from(test(name ? name : "")));
            vscode.workspace.fs.writeFile(
              fileIndex,
              Buffer.from(indexPage(name ? name : ""))
            );
          }
        });
      }
    )
  );
  //For Create Redux Modules
  context.subscriptions.push(
    vscode.commands.registerCommand(
      "fs.newModule",
      async (rootUri: vscode.Uri, _) => {
        let options = {
          ignoreFocusOut: true,
          placeHolder: "Enter Redux Module Name",
        };
        let input = vscode.window.showInputBox(options, cancelToken.token);
        input.then((res) => {
          if (res) {
            const name = Validate(res);
            const fileName = vscode.Uri.parse(`${rootUri}/${name}/index.js`);
            vscode.workspace.fs.writeFile(fileName, Buffer.from(reduxModule()));
          }
        });
      }
    )
  );
}
function Validate(name: string): string | undefined {
  let newName = "";
  words(name).forEach(word => newName += upperFirst(word))
  return newName;
}
function words(word: string): Array<string> {
  // * exclude start and end white-space
  word.trim();
  let words = new Array();
  // ? word contain spaces ?
  if (word.includes(' ')) {
    words = word.split(' ')
  } else if (word.includes('-')) {// ? word contain - ?
    words = word.split('-')
  } else {
    words.push(word)
  }
  return words;
}
function upperFirst(word: string): string | undefined {
  word.toLowerCase();
  let newWord = word.charAt(0).toUpperCase();
  newWord += word.substr(1, word.length - 1);
  return newWord
}
// this method is called when your extension is deactivated
export function deactivate() { }
