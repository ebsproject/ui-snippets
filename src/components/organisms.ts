export function organism(name: string) {
  return `import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, Typography } from '@material-ui/core';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ${name}Organism = React.forwardRef(({ }, ref) => {

  return (
    /* 
     @prop data-testid: Id to use inside ${name.toLowerCase()}.test.js file.
     */
    <Grid
      ref={ref}
      data-testid={'${name}TestId'}
      className='grid grid-cols-2 gap-4 md:grid-cols-6 bg-ebs-brand-default'
    >
      <Grid className='bg-ebs-brand-900 text-white'>
        <Typography variant='body1'>
          <FormattedMessage id='none' defaultMessage='My organism' />
        </Typography>
      </Grid>
    </Grid>
  )
})
// Type and required properties
${name}Organism.propTypes = {}
// Default properties
${name}Organism.defaultProps = {}

export default ${name}Organism
`;
}

export function indexOrganism(name: string) {
  return `export { default } from './${name}';
`;
}
