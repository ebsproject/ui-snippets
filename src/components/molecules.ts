export function molecule(name: string) {
  return `import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import { Box, Typography } from '@material-ui/core';

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ${name}Molecule = React.forwardRef(({ }, ref) => {

  return (
    /* 
     @prop data-testid: Id to use inside ${name}.test.js file.
     */
    <Box
      component='div' 
      ref={ref}
      data-testid={'${name}TestId'}
      className='flex flex-row bg-ebs-brand-default'
    >
      <Box component='div' className="w-1/2 bg-ebs-brand-900">
        <Typography variant='body1'>
          <FormattedMessage id='none' defaultMessage='My molecule' />
        </Typography>
      </Box>
    </Box>
  )
})
// Type and required properties
${name}Molecule.propTypes = {}
// Default properties
${name}Molecule.defaultProps = {}

export default ${name}Molecule
`;
}

export function indexMolecule(name: string) {
  return `export { default } from './${name}';
`;
}
