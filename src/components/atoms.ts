export function atom(name: string) {
  return `import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import { Button, Typography } from '@material-ui/core';

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ${name}Atom = React.forwardRef(({ }, ref) => {
  
  return (
    /* 
     @prop data-testid: Id to use inside ${name}.test.js file.
     */
    <Button
      data-testid={'${name}TestId'}
      ref={ref}
      variant='contained'
      className='w-224 bg-ebs-brand-default hover:bg-ebs-brand-900 text-white'
      aria-label='Login'
    >
      <Typography variant='button'>
        <FormattedMessage id='none' defaultMessage='My button' />
      </Typography>
    </Button>
  )
})
// Type and required properties
${name}Atom.propTypes = {}
// Default properties
${name}Atom.defaultProps = {}

export default ${name}Atom
`;
}

export function indexAtom(name: string) {
  return `export { default } from './${name}';
`;
}
