export function test(name: string) {
  return `import ${name} from './${name}';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('${name} is in the DOM', () => {
  render(<${name}></${name}>)
  expect(screen.getByTestId('${name}TestId')).toBeInTheDocument();
})
`;
}
