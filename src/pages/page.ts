export function page(name: string) {
  return `import React from 'react'
import PropTypes from 'prop-types'
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import { Typography } from '@material-ui/core';

/*
  @param { }: page props,
*/
export default function ${name}View({ }) {

  /*
  @prop data-testid: Id to use inside ${name.toLowerCase()}.test.js file.
 */
  return (
    <div data-testid={'${name}TestId'}>
      <Typography variant='h5'>
        <FormattedMessage id='none' defaultMessage='My page' />
      </Typography>
    </div>
  )
}
// Type and required properties
${name}View.propTypes = {}
// Default properties
${name}View.defaultProps = {}
`;
};

export function indexPage(name: string) {
  return `export { default } from './${name}';
  `;
};