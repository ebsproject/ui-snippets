# EBS Snippets

_Code snippets to assist the developer in adapting the development pattern for the EBS project_

## Pre requirements 📋

_To load this extension, you need to copy [EBS-snippets]() to your VS Code extensions folder .vscode/extensions. Depending on your platform, it is located in the following folders:_

* [Windows](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#your-extension-folder) %USERPROFILE%\.vscode\extensions
* [macOS](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#your-extension-folder) ~/.vscode/extensions
* [Linux](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#your-extension-folder) ~/.vscode/extensions

## Authors ✒️

_Some authors and collaborators_

* **Salvador Ortega** - *Intitial work* - [salvadorlucas](https://github.com/SalvadorLucas)

